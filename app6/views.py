from django.shortcuts import render, redirect
from .models import OpinionBoard
from .forms import OpinionBoardForm

# Create your views here.
def index(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = OpinionBoardForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            form.save()
            return redirect("confirm")

    # if a GET (or any other method) we'll create a blank form
    form = OpinionBoardForm()
    opinions = OpinionBoard.objects.all()
    return render(request, 'index.html', {"opinions":opinions, 'form': form})

def confirm(request):
    if request.method == "POST":
        if "cancel" in request.POST:
            latest = OpinionBoard.objects.latest("date_time_updated")
            latest.delete()
            return redirect("index")

        else:
            return redirect("index")
    
    else:
        latest = OpinionBoard.objects.latest("date_time_updated")
        return render(request, "confirmation.html", {"userInput": latest})