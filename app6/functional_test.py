from selenium import webdriver
import unittest
import time

class InputTest(unittest.TestCase): #
    def setUp(self): #
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.implicitly_wait(3)
        self.browser.quit()

    def test_cancel_input(self):
        selenium = self.browser
        # Opening the link we want to test
        selenium.get('http://localhost:8000/')
        # find the form element
        author = selenium.find_element_by_id('id_author')
        msg = selenium.find_element_by_id('id_msg')

        submit = selenium.find_element_by_name('submit')

        # Fill the form with data
        author.send_keys('Selenium')
        msg.send_keys('This is an automated test message')

        # submitting the form
        submit.click()
        time.sleep(5)
  
        confirmation_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn("Are you sure you want to input the following data?", confirmation_text)

        cancel = selenium.find_element_by_name('cancel')
        cancel.click()
  
        time.sleep(5)
  
        self.assertIn("Avatar's Story 7 Page", self.browser.title)

if __name__ == '__main__': #
    unittest.main(warnings='ignore') #



